import { Component } from "../Сomponent.js";

export class Visit extends Component {
	constructor(id, doctor, purpose, description, priority, fullName, status) {
		super(doctor, purpose, description, priority)
		this.doctor = doctor;
		this.purpose = purpose;
		this.description = description;
		this.priority = priority;
		this.fullName = fullName;
		this.status = status;
		this.id = id;
	}

	getUrgencyLabel() {
		switch (this.priority) {
			case 'high': return " <span class=\"patient__urgency high-urgency\">Невідкладна</span>"
			case 'normal': return " <span class=\"patient__urgency normal-urgency\">Пріоритетна</span>"
			case 'low': return " <span class=\"patient__urgency low-urgency\">Звичайна</span>"
		}
	}
	getDoctorLabel() {
		switch (this.doctor) {
			case 'Cardiologist': return " <span >Кардіолог</span>"
			case 'Dentist': return " <span >Стоматолог</span>"
			case 'Therapist': return " <span >Терапевт</span>"
		}
	}
	getStatus() {
		switch (this.status) {
			case 'open': return "Відкритий"
			case 'done': return "Закритий"
			default: return 'Відкритий'
		}
	}
	getNewStatus() {
		let select = document.querySelector('#edit-status')
		let selectedValue = select.options[select.selectedIndex].value
		this.status = selectedValue
		switch (this.status) {
			case 'open': return "Відкритий"
			case 'done': return "Закритий"
		}
	}
}