//файл самої сторінки. сторінка в нас складається з компонентів, модалок і карток
import { AuthAPIService } from "./AuthAPIService.js";
import { DoctorAPIService } from "./DoctorAPIService.js";

import { ModalAuth } from "./modal/ModalAuth.js";
import { ModalCreateVisit } from "./modal/ModalCreateVisit.js";
import { ModalUpdateVisit } from "./modal/ModalUpdateVisit.js";

import { VisitCardiologist } from "./visit/VisitCardiologist.js";
import { VisitDentist } from "./visit/VisitDentist.js";
import { VisitTherapist } from "./visit/VisitTherapist.js";

export class Page {
    doctorAPI
    authAPI

    authModal
    createVisitModal
    updateVisitModal

    containerCards

    filterForm
    filterStatus
    filterPriority
    filterSearch

    clearFilterButton
    switchMoveButton
    switchNormalButton

    loginButton
    createVisitButton

    isDraggable
    isNoVisits


    constructor(props) {
        this.doctorAPI = new DoctorAPIService()
        this.authAPI = new AuthAPIService()

        this.createVisitModal = new ModalCreateVisit(props.createVisitModal)
        this.authModal = new ModalAuth(props.authModal)

        this.containerCards = document.querySelector('.patients-wrapper')

        this.filterForm = document.querySelector('.form-left')
        this.filterStatus = document.getElementById('status')
        this.filterPriority = document.getElementById('priority')
        this.filterSearch = document.getElementById('search')

        this.clearFilterButton = document.querySelector('.clear__button')
        this.switchNormalButton = document.querySelector('.btn-toggle__normal');
        this.switchMoveButton = document.querySelector('.btn-toggle__move');


        this.createVisitButton = document.querySelector('.send-form')
        this.loginButton = document.querySelector(".login__form-btn")

        this.isNoVisits = document.querySelector("#no_visits")
        this.isDraggable = false

    }

    get allCards() {
        return document.querySelectorAll('.patient');
    }
    get wrapperCards() {
        return document.querySelectorAll('.wrapper-patient');
    }
    get showMoreButtons() {
        return document.querySelectorAll(".show__more-button");
    }
    get removeVisitButtons() {
        return document.querySelectorAll(".patient__header-close");
    }
    get updateVisitButtons() {
        return document.querySelectorAll(".patient__content-button.edit-btn");
    }

    async render() {
        this.createVisitModal.createDomElement()
        this.authModal.createDomElement()

        this.authAPI.tryLogin(this.authModal.loadMain)

        if (this.authAPI.isLoggedIn()) {
            let cards = await this.doctorAPI.getAllCards()
            this.renderExistingCards(cards)
        }
        this.bindDefault();
    }

    bindDefault() {
        this.filterForm.addEventListener('change', this.filterCards.bind(this));
        this.filterSearch.addEventListener('input', this.filterCards.bind(this))

        this.clearFilterButton.addEventListener('click', this.clearFilter.bind(this))
        this.switchNormalButton.addEventListener('click', this.dragAndDropOff.bind(this))
        this.switchMoveButton.addEventListener('click', this.dragAndDropOn.bind(this))

        this.createVisitButton.addEventListener('click', this.createVisit.bind(this))
        this.loginButton.addEventListener('click', this.auth.bind(this))

        this.bindVisitButtons()
    }

    bindVisitButtons() {
        this.showMoreButtons.forEach(button => {
            if (button.getAttribute('listenerShowMore') !== 'true') {
                button.addEventListener('mousedown', this.toggleCard);
                button.setAttribute('listenerShowMore', 'true');
            }
        });
        this.removeVisitButtons.forEach(button => {
            if (button.getAttribute('listenerRemove') !== 'true') {
                button.addEventListener('mousedown', this.removeVisit.bind(this));
                button.setAttribute('listenerRemove', 'true');
            }
        });
        this.updateVisitButtons.forEach(button => {
            if (button.getAttribute('listenerRemove') !== 'true') {
                button.addEventListener('mousedown', this.editVisit.bind(this));
                button.setAttribute('listenerRemove', 'true');
            }
        });
    }

    filterCards() {
        const selectedStatus = this.filterStatus.value;
        const selectedPriority = this.filterPriority.value;
        const searchText = this.filterSearch.value.toLowerCase();

        this.allCards.forEach(card => {
            const purpose = card.querySelector('.hidden__purpose').textContent.toLowerCase();
            const description = card.querySelector('.hidden__description').textContent.toLowerCase();
            const status = card.querySelector('.patient__status').classList[1];
            const priority = card.querySelector('.patient__urgency').classList[1];

            const isMatch = searchText.length === 0 || purpose.includes(searchText) || description.includes(searchText);
            const isStatusMatch = selectedStatus === '' || status === selectedStatus;
            const isPriorityMatch = selectedPriority === '' || priority === selectedPriority;

            const isVisible = isMatch && isStatusMatch && isPriorityMatch;

            card.style.display = isVisible ? 'block' : 'none';

            const cardWrapper = card.closest('.wrapper-patient');
            if (cardWrapper) {
                cardWrapper.style.display = card.style.display;
            }
        });
    }

    toggleCard(event) {
        const card = event.target.closest('.patient');

        const content = card.querySelector('.hidden__content');

        const isContentHidden = content.classList.contains('hidden');

        content.classList.toggle('hidden');

        event.target.textContent = isContentHidden ? 'Згорнути' : 'Показати більше';
    }

    clearFilter(e) {
        e.preventDefault();
        this.allCards.forEach(card => {
            card.style.display = 'block';
        });
        this.wrapperCards.forEach(wrapper => {
            wrapper.style.display = 'block';
        });
        this.filterStatus.value = '';
        this.filterPriority.value = '';
    }

    dragAndDropOn() {
        this.switchMoveButton.classList.add('active');
        this.switchNormalButton.classList.remove('active');

        this.isDraggable = true;

        this.allCards.forEach((card, index) => {
            if (window.innerWidth < 992) {
                card.addEventListener('touchstart', (e) => {
                    this.dragAndDrop(e, "touch", card, index)
                });
            } else {
                card.addEventListener('mousedown', (e) => {
                    this.dragAndDrop(e, "mouse", card, index)
                });
            }

            card.addEventListener('dragstart', (e) => {
                e.preventDefault();
            });
        });
    }

    dragAndDrop(e, type, card, index) {
        let end
        let x
        let y
        let containerCards = this.containerCards

        if (type === 'mouse') {
            end = 'mouseup'
            x = e.pageX
            y = e.pageY
        } else {
            end = 'touchend'
            x = e.touches[0].clientX
            y = e.touches[0].clientY
        }

        if (!this.isDraggable) return;
        let cardCoord = this.getMouseClickCoordinates(card);
        let shiftX = x - cardCoord.left;
        let shiftY = y - cardCoord.top;

        card.style.position = 'absolute';
        containerCards.append(card);
        moveCardUnderCursor(e);

        card.style.zIndex = index + 2;

        function moveCardUnderCursor(e) {
            if (type === 'mouse') {
                x = e.pageX
                y = e.pageY
            } else {
                x = e.touches[0].clientX
                y = e.touches[0].clientY
            }
            let dropZoneRect = containerCards.getBoundingClientRect();
            let newX = x - shiftX;
            let newY = y - shiftY;
            if (newX >= dropZoneRect.left && newX <= dropZoneRect.right - card.offsetWidth) {
                card.style.left = newX + 'px';
            }
            if (newY >= dropZoneRect.top && newY <= dropZoneRect.bottom - card.offsetHeight) {
                card.style.top = newY + 'px';
            }
        }

        containerCards.addEventListener(type + 'move', moveCardUnderCursor);
        card.addEventListener(end, () => {
            containerCards.removeEventListener(type + 'move', moveCardUnderCursor);
        });
    }

    getMouseClickCoordinates = (card) => {
        let box = card.getBoundingClientRect();
        return {
            top: box.top + window.pageYOffset,
            left: box.left + window.pageXOffset
        };
    };

    dragAndDropOff() {
        this.switchMoveButton.classList.remove('active');
        this.switchNormalButton.classList.add('active');
        this.wrapperCards.forEach(wrapper => {
            this.allCards.forEach(card => {
                if (wrapper.children.length === 0 && wrapper.dataset.wrap === card.dataset.card) {
                    card.style.position = "static";
                    wrapper.append(card)
                }
            })
        })

        this.isDraggable = false;
    }

    async createVisit() {
        let visit = this.createVisitModal.createVisit()
        if (typeof visit === "string") {
            this.createVisitModal.addErrorMessage(visit)
            return
        }
        if (visit !== undefined) {
            this.createVisitModal.removeErrorMessage()

            let resp = await this.doctorAPI.createCard(visit)
            if (resp.id !== undefined) {
                this.isNoVisits.style.display = "none"
                visit.render(resp.id)
                this.bindVisitButtons()
            }
        }
    }

    renderExistingCards(cards) {
        if (cards.length === 0) {
            this.isNoVisits.style.display = "block"
        } else {
            this.isNoVisits.style.display = "none"
        }
        console.log(cards)

        cards.forEach((card, index) => {

            let visit
            switch (card.doctor) {
                case "Therapist":
                    visit = new VisitTherapist(card.id, card.doctor, card.purpose, card.description, card.priority, card.fullName, card.status, card.age)
                    visit.render(card.id)
                    break
                case "Dentist":
                    visit = new VisitDentist(card.id, card.doctor, card.purpose, card.description, card.priority, card.fullName, card.status, card.lastVisitDate)
                    visit.render(card.id)
                    break
                case "Cardiologist":
                    visit = new VisitCardiologist(card.id, card.doctor, card.purpose, card.description, card.priority, card.fullName, card.status, card.normalPressure, card.bodyMassIndex, card.cardiovascularDiseases, card.age)
                    visit.render(card.id)
                    break
            }
        })
    }
    async auth(e) {
        await this.authAPI.sendAuthRequest(e)
        this.authAPI.tryLogin(this.authModal.loadMain)
    }

    async removeVisit(e) {
        let id = e.target.parentElement.parentElement.dataset.card
        let res = await this.doctorAPI.deleteCard(id)
        if (res === true) {
            const visit = document.querySelector(`[data-wrap="${id}"]`);
            visit.remove()
            if (document.querySelector('[data-wrap]') === null) {
                this.isNoVisits.style.display = "block"
            }
        }
    }

    async editVisit(e) {
        let id = e.target.parentElement.parentElement.parentElement.parentElement.dataset.card
        let res = await this.doctorAPI.getCard(id)

        this.updateVisitModal = new ModalUpdateVisit(res)

        this.updateVisitModal.open()

        document.querySelector('.update-form').onclick = this.updateVisitOnPageAndServer.bind(this)
    }

    async updateVisitOnPageAndServer() {
        const updateForm = document.querySelector('.update-form').style.display = 'none';
        document.querySelector('.send-form').style.display = 'block';

        let visit = this.updateVisitModal.createVisit()
        visit.update()

        let res = await this.doctorAPI.editCard(visit)
        this.updateVisitModal = new ModalUpdateVisit(res)

        document.querySelector('#edit-status').style.display = 'none'
    }

}

