# [Step-project 'Cards'](https://sllawwkoo.gitlab.io/step-project-3-cards/)

***
## Project Team:

- [Яременко Ян](https://t.me/yan_yaremenko)
    - ## TO-DO:
        - [x] HTML coding
        - [x] Adaptive
        - [x] JS (part)
        - [x] API
        - [x] Refactoring
        - [x] fixed bugs
***
- [Новак Інгрід](https://t.me/obadaya)
    - ## TO-DO:
        - [x] Class architecture
        - [x] Combined all JS files into one logic
        - [x] API
        - [x] Refactoring
        - [x] fixed bugs
***

- [Пивоваров Вячеслав](https://t.me/sllawwkoo)
    - ## TO-DO:
        - [x] Cards filtration
        - [x] Drag-and-drop
        - [x] API
        - [x] Refactoring
        - [x] fixed bugs
***

## Dependencies

- [GulpJS](https://gulpjs.com/)
- [SASS](https://sass-lang.com/), [SCSS]()
- [HTML]()
- [BEM]()
- [JavaScript]()
- [JavaScript ES6 modules]()
- [rest API]()

### Installing

To run the project you need to install [NodeJS](https://nodejs.org/) v8+.

### 

Run the command to install the dependencies.

```sh
$ npm i
```

### Executing Development server

To start the development server, run the command:

```sh
$ npm run dev
```

### Production build

To start the production build, run the command:

```sh
$ npm run build
```
### Email and password to access the web-page.

```sh
login: secretary@gmail.com
password: secretary 
```

Deployment: https://sllawwkoo.gitlab.io/step-project-3-cards/